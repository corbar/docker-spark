#!/usr/bin/env bash

# set the IP/name for the local host
if [ -z "$SPARK_LOCAL_IP" ]; then
  if [ "$SPARK_NETWORK_INTERFACE" ]; then
      SPARK_LOCAL_IP=`ip addr list ${SPARK_NETWORK_INTERFACE} | grep "inet " | cut -d" " -f6 | cut -d"/" -f1`
  else
      SPARK_LOCAL_IP=`hostname`
  fi
fi

# set the spark master
if [ -z "$SPARK_MASTER_IP" ]; then
  if [ ${SPARK_MASTER} ]; then
    SPARK_MASTER_IP=`getent hosts ${SPARK_MASTER} | awk '{ print $1 }'`
  else
    echo "ERROR: no spark master!!"
    exit 1
  fi
fi

# set the cassandra IP
if [ -z "$CASSANDRA_IP" ] && [ "$CASSANDRA" ]; then
  CASSANDRA_IP=`getent hosts ${CASSANDRA} | awk '{ print $1 }'`
fi


${SPARK_HOME}/bin/spark-shell \
    --master spark://${SPARK_MASTER_IP}:${SPARK_MASTER_PORT}  \
    --conf spark.cassandra.connection.host=${CASSANDRA_IP} \
    --properties-file /spark-defaults.conf \
    --packages datastax:spark-cassandra-connector:2.0.0-RC1-s_2.11 \
    "$@"

