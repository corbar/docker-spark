#!/usr/bin/env bash

# set the IP/name for the local host
if [ -z "$SPARK_LOCAL_IP" ]; then
    if [ "$SPARK_NETWORK_INTERFACE" ]; then
        SPARK_LOCAL_IP=`ip addr list ${SPARK_NETWORK_INTERFACE} | grep "inet " | cut -d" " -f6 | cut -d"/" -f1`
    else
        SPARK_LOCAL_IP=`hostname`
    fi
fi


${SPARK_HOME}/sbin/start-master.sh \
    --properties-file /spark-defaults.conf \
    --host ${SPARK_LOCAL_IP} \
    "$@"

/bin/bash

