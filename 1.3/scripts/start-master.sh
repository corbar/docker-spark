#!/usr/bin/env bash
export SPARK_MASTER_IP=`ip addr list ${SPARK_NETWORK_INTERFACE} | grep "inet " | cut -d" " -f6 | cut -d"/" -f1`
export SPARK_LOCAL_IP=`ip addr list ${SPARK_NETWORK_INTERFACE} | grep "inet " | cut -d" " -f6 | cut -d"/" -f1`

${SPARK_HOME}/sbin/start-master.sh --properties-file /spark-defaults.conf -i $SPARK_LOCAL_IP "$@"

/bin/bash
