#!/usr/bin/env bash


# set the IP/name for the local host
if [ -z "$SPARK_LOCAL_IP" ]; then
    if [ "$SPARK_NETWORK_INTERFACE" ]; then
        SPARK_LOCAL_IP=`ip addr list ${SPARK_NETWORK_INTERFACE} | grep "inet " | cut -d" " -f6 | cut -d"/" -f1`
    else
        SPARK_LOCAL_IP=`hostname`
    fi
fi

# set the spark master
if [ -z "$SPARK_MASTER_IP" ]; then
    if [ ${SPARK_MASTER} ]; then
        SPARK_MASTER_IP=`getent hosts ${SPARK_MASTER} | awk '{ print $1 }'`
    else
        echo "ERROR: no spark master!!"
        exit 1
    fi
fi


${SPARK_HOME}/bin/spark-class org.apache.spark.deploy.worker.Worker \
        spark://${SPARK_MASTER_IP}:${SPARK_MASTER_PORT} \
        --properties-file /spark-defaults.conf \
        --host ${SPARK_LOCAL_IP} \
        "$@"


